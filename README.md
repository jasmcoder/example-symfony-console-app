# Example Symfony Console App

## Requirement
* PHP 7.4
* Docker (optional)

## Installation

Clone the project
```
$ git clone https://xjasmx@bitbucket.org/jasmcoder/example-symfony-console-app.git
```

#### For docker developers
Run the application with makefile:

```
$ make init
```
or
```
$ docker-compose pull
$ docker-compose build
$ docker-compose up -d
$ docker-compose run --rm app-php-cli composer install
```
##### Optional
```
$ docker-compose run --rm app-php-cli bin/phpunit
```

### Console Command App
#### Architecture [Clean Architecture](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
#### Description
The application provides an opportunity to get professional skills for each position and check the existence of a skill for position

##### List of positions:
* developer
* designer
* tester
* manager

##### List of positions skills:
* code writing
* code testing
* communication with manager
* paint
* set task

#### Command of getting a list of position skills
```
$ docker-compose run --rm app-php-cli php bin/console position:skills $position
```
##### Example
Input to console:
```
$ docker-compose run --rm app-php-cli php bin/console position:skills developer 
```
Output: 
```
developer position skills:
==========================

 * code writing
 * code testing
 * communication with manager
```
#### Command of check the existence of a skill for position

```
$ docker-compose run --rm app-php-cli php bin/console position:can $position $skill
```
##### Example
Input to console:
```
$ docker-compose run --rm app-php-cli php bin/console position:can developer codeTesting
```
Output: 
```
developer position skills:
==========================

 true
```

