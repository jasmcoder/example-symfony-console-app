<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Model\Skill;

interface PositionServiceInterface
{
    /**
     * @param string $positionType
     * @return Skill[]
     */
    public function getPositionSkills(string $positionType): array;

    public function isPresentSkillOfPosition(string $positionType, string $skillType): bool;
}
