<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Model\Exception\NotFoundException;
use App\Domain\Model\Position;
use App\Domain\Model\PositionRepositoryInterface;

class PositionService implements PositionServiceInterface
{
    private PositionRepositoryInterface $positionRepository;

    public function __construct(PositionRepositoryInterface $positionRepository)
    {
        $this->positionRepository = $positionRepository;
    }

    /**
     * @inheritDoc
     * @throws NotFoundException
     */
    public function getPositionSkills(string $positionType): array
    {
        $position = $this->getPosition($positionType);

        return $position->getSkills();
    }

    /**
     * @param string $positionType
     * @param string $skillType
     * @return bool
     * @throws NotFoundException
     */
    public function isPresentSkillOfPosition(string $positionType, string $skillType): bool
    {
        $position = $this->getPosition($positionType);

        return $position->isPresentSkill($skillType);
    }

    /**
     * @param string $positionType
     * @return Position
     * @throws NotFoundException
     */
    private function getPosition(string $positionType): Position
    {
        /** @var Position|null $employee */
        $position = $this->positionRepository->findPosition($positionType);
        if (!$position) {
            throw new NotFoundException("$positionType position not found.");
        }
        return $position;
    }
}
