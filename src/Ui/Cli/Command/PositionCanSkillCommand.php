<?php

declare(strict_types=1);

namespace App\Ui\Cli\Command;

use App\Domain\Model\Exception\NotFoundException;
use App\Infrastructure\StringHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PositionCanSkillCommand extends BasePositionCommand
{
    protected static $defaultName = 'position:can';

    protected function configure(): void
    {
        $this
            ->setDescription('Check if the position has a skill.')
            ->addArgument('position', InputArgument::REQUIRED, 'Employee position')
            ->addArgument('skill', InputArgument::REQUIRED, 'Position skill');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $positionType = $input->getArgument('position');
        $skillType = StringHelper::camelCaseToString(
            $input->getArgument('skill'),
            StringHelper::LETTERS_LOWER_CASE
        );

        try {
            $isPresentSkillOfPosition = $this->positionService->isPresentSkillOfPosition($positionType, $skillType);
            $io->title("$positionType position skills:");
            $io->text($isPresentSkillOfPosition ? 'true' : 'false');
        } catch (NotFoundException $e) {
            $io->error($e->getMessage());
            return self::FAILURE;
        }

        return self::SUCCESS;
    }
}
