<?php

declare(strict_types=1);

namespace App\Ui\Cli\Command;

use App\Domain\Model\Exception\NotFoundException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PositionSkillsListCommand extends BasePositionCommand
{
    protected static $defaultName = 'position:skills';

    protected function configure(): void
    {
        $this
            ->setDescription('Conclusion of position skills.')
            ->addArgument('position', InputArgument::REQUIRED, 'Employee position');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $positionType = $input->getArgument('position');

        try {
            $skillsEmployee = $this->positionService->getPositionSkills($positionType);
            $io->title("$positionType position skills:");
            $io->listing($skillsEmployee);
        } catch (NotFoundException $e) {
            $io->error($e->getMessage());
            return self::FAILURE;
        }

        return self::SUCCESS;
    }
}
