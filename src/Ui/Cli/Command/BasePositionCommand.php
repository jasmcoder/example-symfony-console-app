<?php

declare(strict_types=1);

namespace App\Ui\Cli\Command;

use App\Application\PositionServiceInterface;
use Symfony\Component\Console\Command\Command;

abstract class BasePositionCommand extends Command
{
    public const SUCCESS = 0;
    public const FAILURE = 1;

    protected PositionServiceInterface $positionService;

    public function __construct(PositionServiceInterface $positionService)
    {
        $this->positionService = $positionService;
        parent::__construct();
    }
}
