<?php

declare(strict_types=1);

namespace App\Domain\Model;

class Skill
{
    private string $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function equals(string $skillType): bool
    {
        return $this->getType() === $skillType;
    }

    public function __toString(): string
    {
        return $this->type;
    }
}
