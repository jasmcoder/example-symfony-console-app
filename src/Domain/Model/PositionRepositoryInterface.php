<?php

declare(strict_types=1);

namespace App\Domain\Model;

interface PositionRepositoryInterface
{
    public function findPosition(string $positionType): ?Position;
}
