<?php

declare(strict_types=1);

namespace App\Domain\Model;

class Position
{
    private string $type;

    /**
     * @return Skill[]
     */
    private array $skills = [];

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Skill[]
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    public function isPresentSkill(string $skillType): bool
    {
        /** @var Skill $skill */
        foreach ($this->skills as $skill) {
            if ($skill->equals($skillType)) {
                return true;
            }
        }

        return false;
    }

    public function addSkill(Skill $skill): void
    {
        /** @var Skill $skill */
        foreach ($this->skills as $presentSkill) {
            if ($presentSkill->equals($skill->getType())) {
                throw new \InvalidArgumentException("Skill {$skill->getType()} is present.");
            }
        }
        $this->skills[] = $skill;
    }
}
