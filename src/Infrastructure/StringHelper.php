<?php

declare(strict_types=1);

namespace App\Infrastructure;

class StringHelper
{
    public const LETTERS_DEFAULT_CASE = 'default';
    public const LETTERS_LOWER_CASE = 'lower';
    public const LETTERS_UPPER_CASE = 'upper';

    public static function camelCaseToString(string $string, string $caseLetters = self::LETTERS_DEFAULT_CASE): string
    {
        $pieces = preg_split('/(?=[A-Z])/', $string);
        if ($caseLetters === self::LETTERS_LOWER_CASE) {
            return strtolower(implode(' ', $pieces));
        }

        if ($caseLetters === self::LETTERS_UPPER_CASE) {
            return strtoupper(implode(' ', $pieces));
        }

        return implode(' ', $pieces);
    }
}
