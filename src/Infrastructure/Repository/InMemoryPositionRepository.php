<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Model\Position;
use App\Domain\Model\PositionRepositoryInterface;
use App\Infrastructure\PositionFactory;

class InMemoryPositionRepository implements PositionRepositoryInterface
{
    public function findPosition(string $positionType): ?Position
    {
        return PositionFactory::create($positionType);
    }
}
