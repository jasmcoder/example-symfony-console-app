<?php

declare(strict_types=1);

namespace App\Infrastructure;

interface PositionRegistry
{
    public const POSITION_DEVELOPER = 'developer';
    public const POSITION_DESIGNER = 'designer';
    public const POSITION_TESTER = 'tester';
    public const POSITION_MANGER = 'manager';

    public const SKILL_CODE_WRITING = 'code writing';
    public const SKILL_CODE_TESTING = 'code testing';
    public const SKILL_COMMUNICATION_WITH_MANAGER = 'communication with manager';
    public const SKILL_PAINT = 'paint';
    public const SKILL_SET_TASK = 'set task';

    public const POSITION_SKILLS = [
        self::POSITION_DEVELOPER => [
            self::SKILL_CODE_WRITING,
            self::SKILL_CODE_TESTING,
            self::SKILL_COMMUNICATION_WITH_MANAGER
        ],
        self::POSITION_DESIGNER => [
            self::SKILL_PAINT,
            self::SKILL_COMMUNICATION_WITH_MANAGER
        ],
        self::POSITION_TESTER => [
            self::SKILL_CODE_TESTING,
            self::SKILL_COMMUNICATION_WITH_MANAGER,
            self::SKILL_SET_TASK
        ],
        self::POSITION_MANGER => [
            self::SKILL_SET_TASK
        ]
    ];
}
