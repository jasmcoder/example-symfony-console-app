<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Model\Position;
use App\Domain\Model\Skill;

class PositionFactory
{
    public static function create(string $positionType): ?Position
    {
        if (!array_key_exists($positionType, PositionRegistry::POSITION_SKILLS)) {
            return null;
        }

        $position = new Position($positionType);

        self::setPositionSkill($positionType, $position);

        return $position;
    }

    /**
     * @param string $positionType
     * @param Position $position
     */
    private static function setPositionSkill(string $positionType, Position $position): void
    {
        $positionSkills = PositionRegistry::POSITION_SKILLS[$positionType];

        foreach ($positionSkills as $skill) {
            $position->addSkill(new Skill($skill));
        }
    }
}
