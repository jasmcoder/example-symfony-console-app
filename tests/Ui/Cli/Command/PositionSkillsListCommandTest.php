<?php

declare(strict_types=1);

namespace App\Tests\Ui\Cli\Command;

use App\Infrastructure\PositionRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PositionSkillsListCommandTest extends KernelTestCase
{
    private const NAME = 'position:skills';
    private Application $application;

    public function setUp(): void
    {
        parent::setUp();
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
    }

    public function testSuccessful(): void
    {
        $command = $this->application->find(self::NAME);

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'position' => PositionRegistry::POSITION_DEVELOPER
        ]);

        $output = $commandTester->getDisplay();

        self::assertStringContainsString(
            <<<EOD
developer position skills:
==========================

 * code writing
 * code testing
 * communication with manager
EOD,
            $output
        );
    }

    public function testFailed(): void
    {
        $command = $this->application->find(self::NAME);

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'position' => 'other position',
        ]);

        $output = $commandTester->getDisplay();

        self::assertStringContainsString('[ERROR] other position position not found.', $output);
    }
}
