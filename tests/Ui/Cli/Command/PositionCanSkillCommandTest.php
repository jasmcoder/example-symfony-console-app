<?php

declare(strict_types=1);

namespace App\Tests\Ui\Cli\Command;

use App\Infrastructure\PositionRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PositionCanSkillCommandTest extends KernelTestCase
{
    private const NAME = 'position:can';
    private Application $application;

    public function setUp(): void
    {
        parent::setUp();
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
    }

    public function testSuccessfulCheckForExistence(): void
    {
        $command = $this->application->find(self::NAME);

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'position' => PositionRegistry::POSITION_DEVELOPER,
            'skill' => PositionRegistry::SKILL_CODE_WRITING
        ]);

        $output = $commandTester->getDisplay();

        self::assertStringContainsString('true', $output);
    }

    public function testSuccessfulCheckForNonexistence(): void
    {
        $command = $this->application->find(self::NAME);

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'position' => PositionRegistry::POSITION_DEVELOPER,
            'skill' => PositionRegistry::SKILL_PAINT
        ]);

        $output = $commandTester->getDisplay();

        self::assertStringContainsString('false', $output);
    }

    public function testFailed(): void
    {
        $command = $this->application->find(self::NAME);

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'position' => 'other position',
            'skill' => PositionRegistry::SKILL_PAINT
        ]);

        $output = $commandTester->getDisplay();

        self::assertStringContainsString('[ERROR] other position position not found.', $output);
    }
}
