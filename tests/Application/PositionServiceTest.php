<?php

declare(strict_types=1);

namespace App\Tests\Application;

use App\Application\PositionService;
use App\Application\PositionServiceInterface;
use App\Domain\Model\Exception\NotFoundException;
use App\Infrastructure\PositionRegistry;
use App\Infrastructure\Repository\InMemoryPositionRepository;
use PHPUnit\Framework\TestCase;

class PositionServiceTest extends TestCase
{
    private PositionServiceInterface $positionService;

    public function setUp(): void
    {
        parent::setUp();
        $this->positionService = new PositionService(new InMemoryPositionRepository());
    }

    public function testSuccessfulAcquisitionOfPositionSkills(): void
    {
        $developerSkills = $this->positionService->getPositionSkills(PositionRegistry::POSITION_DEVELOPER);

        self::assertEquals(PositionRegistry::POSITION_SKILLS[PositionRegistry::POSITION_DEVELOPER], $developerSkills);
        self::assertNotEquals(PositionRegistry::POSITION_SKILLS[PositionRegistry::POSITION_TESTER], $developerSkills);
    }

    public function testFailedAcquisitionOfPositionSkillsWhenPassingANonexistentPositionType(): void
    {
        $this->expectException(NotFoundException::class);

        $this->positionService->getPositionSkills('other position');
    }

    public function testSuccessfulCheckTheExistenceOfASkillForAPosition(): void
    {
        $skillOfPositionTrue = $this->positionService->isPresentSkillOfPosition(
            PositionRegistry::POSITION_TESTER,
            PositionRegistry::SKILL_CODE_TESTING
        );
        $skillOfPositionFalse = $this->positionService->isPresentSkillOfPosition(
            PositionRegistry::POSITION_TESTER,
            PositionRegistry::SKILL_CODE_WRITING
        );

        self::assertTrue($skillOfPositionTrue);
        self::assertFalse($skillOfPositionFalse);
    }

    public function testFailedCheckTheExistenceOfASkillForAPositionWhenPassingANonexistentPositionType(): void
    {
        $this->expectException(NotFoundException::class);

        $this->positionService->isPresentSkillOfPosition(
            'other position',
            PositionRegistry::SKILL_CODE_TESTING
        );
    }
}
