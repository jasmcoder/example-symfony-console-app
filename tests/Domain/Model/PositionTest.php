<?php

declare(strict_types=1);

namespace App\Tests\Domain\Model;

use App\Domain\Model\Position;
use App\Domain\Model\Skill;
use App\Infrastructure\PositionRegistry;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;

class PositionTest extends TestCase
{
    public function testSuccessfulAddSkills(): void
    {
        $position = new Position(PositionRegistry::POSITION_DEVELOPER);

        $skills = [
            new Skill(PositionRegistry::SKILL_CODE_WRITING),
            new Skill(PositionRegistry::SKILL_CODE_TESTING),
            new Skill(PositionRegistry::SKILL_COMMUNICATION_WITH_MANAGER)
        ];

        foreach ($skills as $skill) {
            $position->addSkill($skill);
        }

        self::assertEquals($skills, $position->getSkills());
    }

    public function testFailedAddDuplicateSkills(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $position = new Position(PositionRegistry::POSITION_DEVELOPER);

        $skills = [
            new Skill(PositionRegistry::SKILL_CODE_WRITING),
            new Skill(PositionRegistry::SKILL_CODE_WRITING),
            new Skill(PositionRegistry::SKILL_COMMUNICATION_WITH_MANAGER)
        ];

        foreach ($skills as $skill) {
            $position->addSkill($skill);
        }

        self::assertEquals($skills, $position->getSkills());
    }

    public function testSuccessfulGetType(): void
    {
        $position = new Position(PositionRegistry::POSITION_DEVELOPER);
        assertEquals(PositionRegistry::POSITION_DEVELOPER, $position->getType());
    }
}
