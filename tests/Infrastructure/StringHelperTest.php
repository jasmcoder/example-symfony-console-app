<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use App\Infrastructure\StringHelper;
use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{
    private const INPUT_STR = 'helloWorld';

    public function testCamelCaseToStringDefault(): void
    {
        $outputStr = 'hello World';
        $camelCaseToString = StringHelper::camelCaseToString(self::INPUT_STR);
        self::assertEquals($camelCaseToString, $outputStr);
    }

    public function testCamelCaseToStringUpper(): void
    {
        $outputStr = 'HELLO WORLD';
        $camelCaseToString = StringHelper::camelCaseToString(self::INPUT_STR, StringHelper::LETTERS_UPPER_CASE);
        self::assertEquals($camelCaseToString, $outputStr);
    }

    public function testCamelCaseToStringLower(): void
    {
        $outputStr = 'hello world';
        $camelCaseToString = StringHelper::camelCaseToString(self::INPUT_STR, StringHelper::LETTERS_LOWER_CASE);
        self::assertEquals($camelCaseToString, $outputStr);
    }
}
